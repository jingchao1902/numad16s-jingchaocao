package edu.neu.madcourse.jingchaocao.wordgame;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.jingchaocao.R;


public class WordGameControlFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.wordgame_play_control_fragment, container, false);
        View confirm = rootView.findViewById(R.id.wordgame_button_confirm);
        View main = rootView.findViewById(R.id.wordgame_button_return);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((WordGameActivity) getActivity()).submitWord(); // type needs to be WordGameActivity
            }
        });
        return rootView;
    }
}