package edu.neu.madcourse.jingchaocao.wordgame;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import edu.neu.madcourse.jingchaocao.R;

public class WordGameActivity extends Activity{
    private WordGameFragment mWordGameFragment;
    private MediaPlayer mMediaPlayer;
    private boolean isActive = true;
    public static final String PREF_RESTORE = "pref_restore";
    private static String TAG = "WordGameActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordgame_play_activity);
        mWordGameFragment = (WordGameFragment) getFragmentManager()
                .findFragmentById(R.id.wordgame_fragment_play_board);
        ImageView imgView = (ImageView) findViewById(R.id.wordgame_sound_image);
        Log.d(TAG, mWordGameFragment.toString());
        Log.d(TAG, imgView.toString());
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMediaPlayer.stop();
            }
        });
        ImageView pauseImgView = (ImageView) findViewById(R.id.wordgame_pause_image);
        pauseImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isActive) {
                    onPause();
                } else {
                    onResume();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // uncopyrighted music from DesiredMusic on soundcloud.com
        mMediaPlayer = MediaPlayer.create(WordGameActivity.this, R.raw.background_audio_game);
        mMediaPlayer.setVolume(0.5f, 0.5f);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }
    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        mWordGameFragment.countDownTimer.cancel();
        String gameData = mWordGameFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE, gameData)
                .commit();
        Log.d("UT3", "state = " + gameData);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void submitWord(){
        mWordGameFragment.submitWord();
    }
}
