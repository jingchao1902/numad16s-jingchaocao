package edu.neu.madcourse.jingchaocao.wordgame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.jingchaocao.R;

public class WordGameMenuActivity extends Activity{
    private AlertDialog acknowDialog;
    private AlertDialog helpDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordgame_menu_activity);

        // new game button
        Button new_button = (Button) findViewById(R.id.wordgame_new_button);
        new_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordGameMenuActivity.this, WordGameActivity.class);
                startActivity(intent);
            }
        });

        // help button
        final Button help_button = (Button) findViewById(R.id.wordgame_help_button);
        help_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(WordGameMenuActivity.this);
                builder.setMessage(R.string.wordgame_help_message);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                helpDialog = builder.show();
            }
        });

        // acknow button
        Button acknow_button = (Button) findViewById(R.id.wordgame_about_button);
        acknow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(WordGameMenuActivity.this);
                builder.setMessage(R.string.wordgame_acknowledgements);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                acknowDialog = builder.show();
            }
        });

        Button quit_button = (Button) findViewById(R.id.wordgame_quit_button);
        quit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
