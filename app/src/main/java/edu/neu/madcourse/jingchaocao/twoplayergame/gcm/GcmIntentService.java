package edu.neu.madcourse.jingchaocao.twoplayergame.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.jingchaocao.R;
import edu.neu.madcourse.jingchaocao.twoplayergame.TwoPlayerGameActivity;
import edu.neu.madcourse.jingchaocao.twoplayergame.TwoPlayerLogActivity;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    static final String TAG = "GCM_Communication";

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String alertText = CommunicationConstants.alertText;
        String titleText = CommunicationConstants.titleText;
        String contentText = CommunicationConstants.contentText;

        Bundle extras = intent.getExtras();

        //Log.d("Receive message: " + String.valueOf(extras.size()), extras.toString());
        if (!extras.isEmpty()) {
            String content = extras.getString("contentText");
            String opponentName = extras.getString("senderName");
            String opponentID = extras.getString("senderID");
            String boardInfo = extras.getString("gameboard");
            String move = extras.getString("move");
            String foundWord =extras.getString("foundWord");
            String connection = extras.getString("connection");
            if (content != null) {
                Log.d(TAG, "Received msg in service: " + content);
                if (content.equals(CommunicationConstants.GAME_REQUEST_MSG)) {
                    if (opponentName != null) {
                        Log.d(TAG, "Opponent name: " + opponentName);
                        if (opponentID != null) {
                            Log.d(TAG, "Opponent id: " + opponentID);
                            storeOpponentInfo(opponentName, opponentID);
                            acceptGameRequest(CommunicationConstants.GAME_REQUEST_ACCEPT, opponentID);
                        }
                        startNewGame(getOpponentName(), boardInfo);
                        //sendNotification("Game request", "Game request", "A game invitation from " + name);
                    }
                } else if (content.equals(CommunicationConstants.GAME_REQUEST_ACCEPT)) {
                    storeOpponentInfo(opponentName, opponentID);
                    startNewGame(getRegistrationName(), "");
                }
            }
//            if (connection != null && foundWord != null){
//                sendNotification("Notification", "New word from " + getOpponentName(), "Open now");
//            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.

    public void sendNotification(String alertText, String titleText,
                                 String contentText) {
        mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent gameIntent = new Intent(this, TwoPlayerGameActivity.class);
        gameIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent intent = PendingIntent.getActivity(this, 0, gameIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setSmallIcon(R.drawable.ic_stat_cloud)
                .setContentTitle(titleText)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(contentText))
                .setContentText(contentText).setTicker(alertText)
                .setAutoCancel(true);
        mBuilder.setContentIntent(intent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        acceptReconnectRequest("reconnect", getOpponentId());
    }

    public void acceptReconnectRequest(final String move, String id) {
        final String reg_device = id;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Notification");
                msgParams.put("data.titleText", "Notification Title");
                msgParams.put("data.moveText", move);
                msgParams.put("data.currentPlayerName", getOpponentName());
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                //msgParams.put("data.currentPlayerName", getOpponentName());
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        getApplicationContext());
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    public void acceptGameRequest(final String message, String id) {
        final String reg_device = id;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Notification");
                msgParams.put("data.titleText", "Notification Title");
                msgParams.put("data.contentText", message);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                msgParams.put("data.senderID", getRegistrationId());
                msgParams.put("data.senderName", getRegistrationName());
                //msgParams.put("data.currentPlayerName", getOpponentName());
                Log.d(TAG, "Sending content: " + message);
                Log.d(TAG, "Sender ID: " + getRegistrationId());
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        getApplicationContext());
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    public String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("registration_id", "");
    }

    public String getRegistrationName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("registration_name", "");
    }

    public String getOpponentName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("opponent_name", "");
    }

    public String getOpponentId() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("opponent_id", "");
    }

    public SharedPreferences getGCMPreferences() {
        return this.getSharedPreferences(TwoPlayerLogActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    public void storeOpponentInfo(String name, String id) {
        final SharedPreferences prefs = getGCMPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("opponent_id", id);
        editor.putString("opponent_name", name);
        editor.apply();
    }

    private void startNewGame(String name, String boardInfo) {
        Intent gameIntent = new Intent(this, TwoPlayerGameActivity.class);
        gameIntent.putExtra("currentPlayerName", name);
        gameIntent.putExtra("boardInfo", boardInfo);
        gameIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(gameIntent);
    }
}
