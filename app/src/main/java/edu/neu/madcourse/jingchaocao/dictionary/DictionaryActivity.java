package edu.neu.madcourse.jingchaocao.dictionary;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import edu.neu.madcourse.jingchaocao.R;

public class DictionaryActivity extends AppCompatActivity {

    private TextView mTextView;
    private EditText mEditText;
    private AlertDialog mDialog;
    private ArrayList<String> wordListDisplay = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private String wordListString;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dictionary_main_screen);

        // acknowButton
        Button acknowButton = (Button) findViewById(R.id.dictionary_acknow_button);
        acknowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DictionaryActivity.this);
                builder.setTitle("Acknowledgements");
                builder.setMessage(R.string.dic_acknowledgment);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
                ((TextView)mDialog.findViewById(android.R.id.message))
                        .setMovementMethod(LinkMovementMethod.getInstance());
            }
        });

        mEditText = (EditText) findViewById(R.id.dictionary_search_editext);
        mTextView = (TextView) findViewById(R.id.dictonary_textView);

        // check if the sharedpreferences is empty
        sharedPreferences = getSharedPreferences("dic_sharedPreferences", MODE_PRIVATE);
        String wordString = sharedPreferences.getString("wordString", null);
        if (wordString == null){
            long start = System.currentTimeMillis();
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.wordlist);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buff = new byte[3145728];
                // 3mb size -> 3145728 bytes (since there are 432334 words) We can estimate every word_unselected
                // has less than 10 length of characters
                for (int i; (i = inputStream.read(buff)) != -1;){
                    byteArrayOutputStream.write(buff, 0, i);
                }
                // Just store the wordlist in a long string, which can avoid using ArrayList
                wordListString = byteArrayOutputStream.toString();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            long end = System.currentTimeMillis();
            double t = ((end - start) / 1000.0);
            System.out.println("read words list and take " + t + " seconds");
        }
        else {
            wordListString = wordString;
        }
        isWord();
        display();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sharedPreferences = getSharedPreferences("dic_sharedPreferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("wordString", wordListString);
        editor.apply();
    }

    public void isWord(){
        final MediaPlayer mediaPlayer = MediaPlayer.create(this,
                R.raw.word_beep);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 3) {
                    String inputWord = s.toString().toLowerCase();
                    String newLine = "\n";
                    String inputWordCheck = newLine.concat(inputWord.concat("\n"));
                    if (newLine.concat(wordListString).contains(inputWordCheck)) {
                        wordListDisplay.add(inputWord);
                        display();
                        mediaPlayer.start();
                    }
                }

            }
        });
    }
    public void display(){
        StringBuilder sb = new StringBuilder();
        for (String s: wordListDisplay){
            if (s != null){
                sb.append(s);
                sb.append("\n");
            }
        }
        mTextView.setText(sb.toString());
    }

    // clear word_unselected list
    public void clear(View view){
        mTextView.setText("");
        mEditText.setText("");
        wordListDisplay.clear();
    }

    // return to menu button
    public void returnToMenu(View view){
        finish();
    }

}