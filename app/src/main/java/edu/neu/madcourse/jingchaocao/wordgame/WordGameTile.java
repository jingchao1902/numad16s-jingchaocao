package edu.neu.madcourse.jingchaocao.wordgame;

import android.view.View;

import java.util.List;

public class WordGameTile {

    private View mView;
    private WordGameTile mSubWordGameTiles[];
    private List<String> currentList;
    private final WordGameFragment mGame;
    private boolean isClicked = false;

    // variables for 9 large boards
    private boolean isSubmitted;

    // variables for 9 small boards in every large board
    private int indexLarge;
    private int indexSmall;

    // variables for word text
    private String word;

    public WordGameTile(WordGameFragment game) {
        this.mGame = game;
    }
    public WordGameFragment getGameFragment() {
        return mGame;
    }
    public void setView(View view) {
        this.mView = view;
    }
    public View getView() {
        return mView;
    }
    public boolean getClickedState() {return isClicked;}
    public void setClickedState() {isClicked = !isClicked;}

    // get positions of small boards
    public int getLargePos(){
       return  indexLarge;
    }
    public int getSmallPos() {
        return indexSmall;
    }

    // set positions of small boards
    public void setLargePos(int pos){
        indexLarge = pos;
    }
    public void setSmallPos(int pos){
        indexSmall = pos;
    }

    // set text of this tile
    public void setWord(String str){ word = str;}
    public String getWord(){ return word;}

    public void setSubTiles(WordGameTile[] subWordGameTiles) {
        this.mSubWordGameTiles = subWordGameTiles;
    }
    public WordGameTile[] getSubTiles() {
        return mSubWordGameTiles;
    }

    public void addWord(){

    }
//    public boolean checkExist(String wordList){
//
//    }

    public void fillLongestWord(String str){

    }
}
