package edu.neu.madcourse.jingchaocao.twoplayergame;

import android.view.View;

import java.util.List;

public class TwoPlayerTile {

    private View mView;
    private TwoPlayerTile mSubTwoPlayerTiles[];
    private List<String> currentList;
    private final TwoPlayerGameFragment mGame;
    private boolean isClicked = false;

    // variables for 9 large boards
    private boolean isSubmitted;

    // variables for 9 small boards in every large board
    private int indexLarge;
    private int indexSmall;

    // variables for word text
    private String word;

    public TwoPlayerTile(TwoPlayerGameFragment game) {
        this.mGame = game;
    }
    public TwoPlayerGameFragment getGameFragment() {
        return mGame;
    }
    public void setView(View view) {
        this.mView = view;
    }
    public View getView() {
        return mView;
    }
    public boolean getClickedState() {return isClicked;}
    public void setClickedState() {isClicked = !isClicked;}

    // get positions of small boards
    public int getLargePos(){
       return  indexLarge;
    }
    public int getSmallPos() {
        return indexSmall;
    }

    // set positions of small boards
    public void setLargePos(int pos){
        indexLarge = pos;
    }
    public void setSmallPos(int pos){
        indexSmall = pos;
    }

    // set text of this tile
    public void setWord(String str){ word = str;}
    public String getWord(){ return word;}

    public void setSubTiles(TwoPlayerTile[] subTwoPlayerTiles) {
        this.mSubTwoPlayerTiles = subTwoPlayerTiles;
    }
    public TwoPlayerTile[] getSubTiles() {
        return mSubTwoPlayerTiles;
    }

    public void addWord(){

    }
//    public boolean checkExist(String wordList){
//
//    }

    public void fillLongestWord(String str){

    }
}
