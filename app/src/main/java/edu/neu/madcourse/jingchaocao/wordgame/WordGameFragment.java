package edu.neu.madcourse.jingchaocao.wordgame;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.jingchaocao.R;

public class WordGameFragment extends Fragment {
    private static final String TAG = "WordGameFragment";
    // dictionary part
    private String wordString;
    private String wordListString;

    //store current word
    private List<WordGameTile> currentWordList = new ArrayList<>();

    private WordGameTile mEntireBoard = new WordGameTile(this);
    private WordGameTile mLargeWordGameTiles[] = new WordGameTile[9];
    private WordGameTile mSmallWordGameTiles[][] = new WordGameTile[9][9];
    private ListView wordListView;
    private TextView currentWordView;
    private List<List<WordGameTile>> foundWordList = new ArrayList<>();
    private List<String> foundWordStrList = new ArrayList<>();
    private List<WordGameTile> selectedWordListForPhaseTwo = new ArrayList<>();
    private int currentPhase = 1;
    ArrayAdapter mAdapter;
    CountDownTimer countDownTimer;
    private long leftTime;

    // store IDs of small and large tiles
    static private int mLargeIds[] = {R.id.wordgame_large1, R.id.wordgame_large2, R.id.wordgame_large3,
            R.id.wordgame_large4, R.id.wordgame_large5, R.id.wordgame_large6, R.id.wordgame_large7, R.id.wordgame_large8,
            R.id.wordgame_large9,};
    static private int mSmallIds[] = {R.id.wordgame_small1, R.id.wordgame_small2, R.id.wordgame_small3,
            R.id.wordgame_small4, R.id.wordgame_small5, R.id.wordgame_small6, R.id.wordgame_small7, R.id.wordgame_small8,
            R.id.wordgame_small9,};

    // pattern for filling longest word_unselected
    private int[] longestWordIndicesArr = {0, 7, 8, 1, 6, 5, 2, 3, 4};

    // scoring part
    private TextView scoreTextView;
    private int score = 0;
    private List<Character> pointOne = new ArrayList<>(Arrays.asList('e', 'a', 'i', 'o', 'n', 'r', 't', 'l', 's'));
    private List<Character> pointTwo = new ArrayList<>(Arrays.asList('d', 'g'));
    private List<Character> pointThree = new ArrayList<>(Arrays.asList('b', 'c', 'm'));
    private List<Character> pointFour = new ArrayList<>(Arrays.asList('f', 'h', 'v', 'w', 'y'));
    private List<Character> pointFive = new ArrayList<>(Arrays.asList('k'));
    private List<Character> pointEight = new ArrayList<>(Arrays.asList('j', 'x'));
    private List<Character> pointTen = new ArrayList<>(Arrays.asList('q', 'z'));

    // available adjacent words
    private int[][] adjacentWordArr = {{1, 3, 4},
            {0, 2, 3, 4, 5},
            {1, 4, 5},
            {0, 1, 4, 6, 7},
            {0, 1, 2, 3, 5, 6, 7, 8},
            {1, 2, 4, 7, 8},
            {3, 4, 7},
            {3, 4, 5, 6, 8},
            {4, 5, 7}};

    //String startMsg = getActivity().getIntent().getStringExtra(CommunicationConstants.GAME_RECEIVED_SERVICE_KEY);
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGame();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.wordgame_play_board_fragment, container, false);
        wordListView = (ListView) rootView.findViewById(R.id.wordgame_wordlistview);
        currentWordView = (TextView) rootView.findViewById(R.id.wordgame_current_word);
        currentWordView.setText("Phase I begins!");
        scoreTextView = (TextView) rootView.findViewById(R.id.wordgame_score_text);
        scoreTextView.setText("Score: " + score);
        getDictionary();
        initViews(rootView);
        return rootView;
    }

    private void initViews(final View rootView) {
        mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            View outer = rootView.findViewById(mLargeIds[large]);
            mLargeWordGameTiles[large].setView(outer);
            mLargeWordGameTiles[large].setLargePos(large);
            int randomIndex = new Random().nextInt(wordListString.length() / 10);
            int start = randomIndex * 10;
            StringBuilder sb = new StringBuilder();
            for (int i = start; i < start + 10; i++) {
                sb.append(wordListString.charAt(i));
            }
            String longestWord = sb.toString();
            Log.d(TAG, longestWord);
            for (int small = 0; small < 9; small++) {
                TextView inner = (TextView) outer.findViewById
                        (mSmallIds[small]);
                inner.setText(Character.toString(longestWord.charAt(longestWordIndicesArr[small])).
                        toUpperCase());
                final WordGameTile smallWordGameTile = mSmallWordGameTiles[large][small];
                final int fLarge = large;
                final int fSmall = small;
                smallWordGameTile.setLargePos(large);
                smallWordGameTile.setSmallPos(small);
                smallWordGameTile.setWord(Character.toString(longestWord.charAt(longestWordIndicesArr[small])).
                        toUpperCase());
                smallWordGameTile.setView(inner);

                // Communication part


                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!smallWordGameTile.getClickedState()) {
                            if (currentWordList.size() == 0) {
                                currentWordList.add(smallWordGameTile);
                                displayCurrentWord();
                                view.setBackgroundResource(R.drawable.word_selected);
                                smallWordGameTile.setClickedState();
                                return;
                            } else if (fLarge != currentWordList.get(0).getLargePos()) {
                                currentWordView.setText("Tap on one board!");
                                return;
                            } else if (fLarge == currentWordList.get(0).getLargePos() && !isLetterAjacent(fSmall)) {
                                currentWordView.setText("Tap on adjacent word!");
                                return;
                            }
                            currentWordList.add(smallWordGameTile);
                            displayCurrentWord();
                            view.setBackgroundResource(R.drawable.word_selected);
                            smallWordGameTile.setClickedState();
                        } else {
                            int index = currentWordList.indexOf(mSmallWordGameTiles[fLarge][fSmall]);
                            Log.d(TAG, "index is: " + index);
                            Log.d(TAG, "list size is " + currentWordList.size());
                            List<WordGameTile> listAfterCurrent = new ArrayList<>();
                            for (int i = index; i < currentWordList.size(); i++) {
                                currentWordList.get(i).setClickedState();
                                int largePos = currentWordList.get(i).getLargePos();
                                int smallPos = currentWordList.get(i).getSmallPos();
                                TextView txtView = (TextView) mLargeWordGameTiles[largePos].getView().findViewById(mSmallIds[smallPos]);
                                txtView.setBackgroundResource(R.drawable.word_unselected);
                                listAfterCurrent.add(currentWordList.get(i));
                            }
                            currentWordList.removeAll(listAfterCurrent);
                            Log.d(TAG, "list size is " + currentWordList.size());
                            displayCurrentWord();
                        }
                    }
                });
            }
        }
        timerStart(rootView);
    }

    public void getDictionary() {
        SharedPreferences msharedPreferences;
        SharedPreferences msharedPreferencesForFillingWords;
        msharedPreferencesForFillingWords = getActivity().getSharedPreferences("wordgame_sharedPreferences_fill", Context.MODE_PRIVATE);
        msharedPreferences = getActivity().getSharedPreferences("wordgame_sharedPreferences", Context.MODE_PRIVATE);
        wordListString = msharedPreferencesForFillingWords.getString("wordListString", wordListString);
        wordString = msharedPreferences.getString("wordString", wordString);
        if (wordString == null) {
            long start = System.currentTimeMillis();
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.wordlist);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buff = new byte[3145728];
                // 3mb size -> 3145728 bytes (since there are 432334 words) We can estimate every word_unselected
                // has less than 10 length of characters
                for (int i; (i = inputStream.read(buff)) != -1; ) {
                    byteArrayOutputStream.write(buff, 0, i);
                }
                // Just store the wordlist in a long string, which can avoid using ArrayList
                wordString = byteArrayOutputStream.toString();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            long end = System.currentTimeMillis();
            double t = ((end - start) / 1000.0);
            System.out.println("read words list and take " + t + " seconds");
            String newLine = "\n";
            wordString = newLine.concat(wordString);
        }
        if (wordListString == null) {
            long start = System.currentTimeMillis();
            // how to transform the sharedPreferences string
            // use hashset ?
            System.out.println(wordString.length());
            try {
                BufferedReader bufReader = new BufferedReader(new StringReader(wordString));
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufReader.readLine()) != null) {
                    if (line.length() == 9) {
                        sb.append(line).append('\n');
                    }
                }
                wordListString = sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            long end = System.currentTimeMillis();
            double t = ((end - start) / 1000.0);
            System.out.println("read " + wordListString.length() + " words and take " + t + " seconds");
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences msharedPreferences;
        msharedPreferences = getActivity().getSharedPreferences("wordgame_sharedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = msharedPreferences.edit();
        editor.putString("wordString", wordString);
        editor.apply();

        SharedPreferences msharedPreferencesForFillingWords;
        msharedPreferencesForFillingWords = getActivity().getSharedPreferences("wordgame_sharedPreferences_fill", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorForFillingWords = msharedPreferencesForFillingWords.edit();
        editorForFillingWords.putString("wordListString", wordListString);
        editorForFillingWords.apply();

    }

    // initialize mEntireBoard, mLargeWordGameTiles, mSmallWordGameTiles
    // bound this game fragment with each tile in this game
    public void initGame() {
        Log.d("UT3", "init game");
        mEntireBoard = new WordGameTile(this);
        // Create all the tiles
        for (int large = 0; large < 9; large++) {
            mLargeWordGameTiles[large] = new WordGameTile(this);
            for (int small = 0; small < 9; small++) {
                mSmallWordGameTiles[large][small] = new WordGameTile(this);
            }
            mLargeWordGameTiles[large].setSubTiles(mSmallWordGameTiles[large]);
        }
        mEntireBoard.setSubTiles(mLargeWordGameTiles);
    }

    public String getState() {
        return "";
    }

    public void submitWord() {
        if (selectedWordListForPhaseTwo.size() == 9 && currentPhase == 2) {
            currentWordView.setText("Phase II begins!");
            setListenersForChooseWords();
            selectedWordListForPhaseTwo.clear();
            return;
        }
        if (currentWordList.size() == 0) {
            currentWordView.setText("Please select a word!");
            return;
        }
        if (currentWordList.size() < 3) {
            currentWordView.setText("Words' length smaller than 3");
            clearSelectedState();
            currentWordList.clear();
            return;
        }
        String word = getCurrentWord();
        Log.d(TAG, "Current word for phase II is: " + word);
        String newLine = "\n";
        String wordToCheck = newLine.concat(word.concat("\n"));
        Log.d(TAG, word);
        Log.d(TAG, wordToCheck);
        if (wordString.contains(wordToCheck)) {
            // add the word into the listview
            StringBuilder sb = new StringBuilder();
            for (WordGameTile wordGameTile : currentWordList) {
                sb.append(wordGameTile.getWord().toLowerCase());
            }
            if (!foundWordStrList.contains(sb.toString())) {
                foundWordList.add(new ArrayList<>(currentWordList));
                foundWordStrList.add(sb.toString());
                mAdapter = new ArrayAdapter<>(getActivity(), R.layout.wordgame_found_word_list, foundWordStrList);
                wordListView.setAdapter(mAdapter);
                wordListView.post(new Runnable() {
                    @Override
                    public void run() {
                        // Select the last row so it will scroll into view...
                        wordListView.setSelection(mAdapter.getCount() - 1);
                    }
                });
                calculateScore(word);
                unEnableSubmittedBoard();
                currentWordView.setText("");
            } else {
                currentWordView.setText("Word already found!");
                clearSelectedState();
                currentWordList.clear();
            }
        } else {
            if (currentPhase == 2) {
                clearSelectedStateForPhaseTwo();
            }
            clearSelectedState();
            currentWordView.setText(R.string.wordgame_word_notfound_text);
            scoreTextView.setText("Score: " + Integer.toString(score -= 1));// penalty for invalid word submission
        }
        if (foundWordList.size() == 9 && currentPhase == 1) {
            currentWordList.clear();
            currentWordView.setText("Select words for Phase II!");
            currentPhase = 2;
            timerStartTwo(mEntireBoard.getView());
            setListenersForSelectWords();
            foundWordList.clear();
            return;
        }
        if (foundWordList.size() != 0 && currentPhase == 2) {
            endCurrentGame();
        }
        currentWordList.clear();
    }

    public void setListenersForSelectWords() {
        Log.d(TAG, "this is a listener for select words!");
        Log.d(TAG, "list size: " + foundWordList.size());
        for (List<WordGameTile> largeWordGameTile : foundWordList) {
            for (WordGameTile smallWordGameTile : largeWordGameTile) {
                Log.d(TAG, "this is a tile!");
                final int largePos = smallWordGameTile.getLargePos();
                final int smallPos = smallWordGameTile.getSmallPos();
                final TextView txtViewPhaseTwo = (TextView) mLargeWordGameTiles[largePos].getView().findViewById(R.id.wordgame_small_2);
                final String selectedWord = mSmallWordGameTiles[largePos][smallPos].getWord();
                TextView txtView = (TextView) mSmallWordGameTiles[largePos][smallPos].getView();
                txtView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "this is a flip!");
                        flipToNextPhase(largePos, smallPos);
                        txtViewPhaseTwo.setText(selectedWord);
                        mLargeWordGameTiles[largePos].setWord(selectedWord);
                        if (!selectedWordListForPhaseTwo.contains(mLargeWordGameTiles[largePos])) {
                            selectedWordListForPhaseTwo.add(mLargeWordGameTiles[largePos]);
                        }
                    }
                });
                txtViewPhaseTwo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        flipToNextPhase(largePos, smallPos);
                        selectedWordListForPhaseTwo.remove(mLargeWordGameTiles[largePos]);
                    }
                });
            }
        }
    }

    public void setListenersForChooseWords() {
        Log.d(TAG, "listeners for choose words!");
        for (int i = 0; i < 9; i++) {
            final int fLarge = i;
            final WordGameTile largeWordGameTile = mLargeWordGameTiles[i];
            final TextView txtViewPhaseTwo = (TextView) mLargeWordGameTiles[i].getView().findViewById(R.id.wordgame_small_2);
            txtViewPhaseTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "this is a listener for large tiles!");
                    if (!largeWordGameTile.getClickedState()) {
                        if (currentWordList.size() == 0) {
                            currentWordList.add(largeWordGameTile);
                            displayCurrentWord();
                            view.setBackgroundResource(R.drawable.word_selected);
                            largeWordGameTile.setClickedState();
                            return;
                        } else if (!isLetterAjacentPhaseTwo(fLarge)) {
                            currentWordView.setText("Tap on adjacent word!");
                            return;
                        }
                        currentWordList.add(largeWordGameTile);
                        displayCurrentWord();
                        view.setBackgroundResource(R.drawable.word_selected);
                        largeWordGameTile.setClickedState();
                    } else {
                        int index = currentWordList.indexOf(largeWordGameTile);
                        Log.d(TAG, "index is: " + index);
                        Log.d(TAG, "list size is " + currentWordList.size());
                        List<WordGameTile> listAfterCurrent = new ArrayList<>();
                        for (int i = index; i < currentWordList.size(); i++) {
                            currentWordList.get(i).setClickedState();
                            final TextView txtViewPhaseTwo = (TextView) currentWordList.get(i).getView().findViewById(R.id.wordgame_small_2);
                            txtViewPhaseTwo.setBackgroundResource(R.drawable.word_unselected);
                            listAfterCurrent.add(currentWordList.get(i));
                        }
                        currentWordList.removeAll(listAfterCurrent);
                        Log.d(TAG, "list size is " + currentWordList.size());
                        displayCurrentWord();
                    }
                }
            });
        }
    }

    public void calculateScore(String word) {
        // if we find a word of length 9 in phase two, then we add 15 points to score.

        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);
            if (pointOne.contains(letter)) {
                score += 1;
            } else if (pointTwo.contains(letter)) {
                score += 2;
            } else if (pointThree.contains(letter)) {
                score += 3;
            } else if (pointFour.contains(letter)) {
                score += 4;
            } else if (pointFive.contains(letter)) {
                score += 5;
            } else if (pointEight.contains(letter)) {
                score += 8;
            } else if (pointTen.contains(letter)) {
                score += 10;
            }
        }
        scoreTextView.setText("Score: " + score);
    }

    public void clearSelectedStateForPhaseTwo() {
        Log.d(TAG, "clear state for phase two!");
        for (WordGameTile word : currentWordList) {
            int largePos = word.getLargePos();
            mLargeWordGameTiles[largePos].getView().findViewById(R.id.wordgame_small_2).setBackgroundResource(R.drawable.word_unselected);
            //mLargeWordGameTiles[largePos].getView().setBackgroundResource(R.drawable.word_unselected);
            mLargeWordGameTiles[largePos].setClickedState();
        }
    }

    public void clearSelectedState() {
        for (WordGameTile word : currentWordList) {
            int largePos = word.getLargePos();
            int smallPos = word.getSmallPos();
            mSmallWordGameTiles[largePos][smallPos].getView().setBackgroundResource(R.drawable.word_unselected);
            mSmallWordGameTiles[largePos][smallPos].setClickedState();
        }
    }

    public void timerStartTwo(final View rootView) {
        countDownTimer = new CountDownTimer(90000, 1000) {
            TextView view = (TextView) rootView.findViewById(R.id.wordgame_timer_text);

            public void onTick(long millisUntilFinished) {
                view.setText("Time: " + formatMilliSecondsToTime(millisUntilFinished));
            }

            public void onFinish() {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Game ends!")
                        .setMessage("Your score is: " + score)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), WordGameMenuActivity.class);
                                getActivity().startActivity(intent);
                            }
                        })
                        .show();
            }
        }.start();
    }

    public void timerStart(final View rootView) {
        countDownTimer = new CountDownTimer(90000, 1000) {
            TextView view = (TextView) rootView.findViewById(R.id.wordgame_timer_text);

            public void onTick(long millisUntilFinished) {
                leftTime = millisUntilFinished;
                Log.d(TAG, "Time left: " + millisUntilFinished);
                if (millisUntilFinished <= 11000 && millisUntilFinished >= 10000) {
                    currentWordView.setText("Only ten seconds left!");
                }
                view.setText("Time: " + formatMilliSecondsToTime(millisUntilFinished));
                if (currentPhase == 2) {
                    this.cancel();
                }
            }

            public void onFinish() {
                // why can't use getActivity() here ?
                new AlertDialog.Builder(getActivity())
                        .setTitle("Time is out")
                        .setMessage("Enter a new game!")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), WordGameMenuActivity.class);
                                getActivity().startActivity(intent);
                            }
                        })
                        .show();
            }
        }.start();
    }

    private String formatMilliSecondsToTime(long milliseconds) {

        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        return twoDigitString(minutes) + " : " + twoDigitString(seconds);
    }

    private String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    public boolean isLetterAjacentPhaseTwo(int large) {
        int lastPos = currentWordList.get(currentWordList.size() - 1).getLargePos();
        int[] currentAvailablePos = adjacentWordArr[lastPos];
        List<Integer> currentAvailablePosList = new ArrayList<>();
        for (int pos : currentAvailablePos) {
            currentAvailablePosList.add(pos);
        }
        for (WordGameTile wordGameTile : currentWordList) {
            int largePos = wordGameTile.getLargePos();
            currentAvailablePosList.remove(Integer.valueOf(largePos));
        }
        return currentAvailablePosList.contains(large);
    }

    public boolean isLetterAjacent(int small) {
        int lastPos = currentWordList.get(currentWordList.size() - 1).getSmallPos();
        int[] currentAvailablePos = adjacentWordArr[lastPos];
        List<Integer> currentAvailablePosList = new ArrayList<>();
        for (int pos : currentAvailablePos) {
            currentAvailablePosList.add(pos);
        }
        for (WordGameTile word : currentWordList) {
            int smallPos = word.getSmallPos();
            Log.d(TAG, "Current small pos: " + small);
            currentAvailablePosList.remove(Integer.valueOf(smallPos));
        }
        return currentAvailablePosList.contains(small);
    }

    public void unEnableSubmittedBoard() {
        int largePos = currentWordList.get(0).getLargePos();
        for (WordGameTile smallWordGameTile : mSmallWordGameTiles[largePos]) {
            if (!currentWordList.contains(smallWordGameTile)) {
                int smallPos = smallWordGameTile.getSmallPos();
                // must use findViewById method to get view type! otherwise cannot access the methods
                // inside this view
                TextView txtView = (TextView) mLargeWordGameTiles[largePos].getView().findViewById(mSmallIds[smallPos]);
                txtView.setText("");
            }
        }
        // make the words unclickable
        for (int smallId : mSmallIds) {
            TextView txtView = (TextView) mLargeWordGameTiles[largePos].getView().findViewById(smallId);
            txtView.setOnClickListener(null);
        }
    }

    public void displayCurrentWord() {
        currentWordView.setText(getCurrentWord());
    }

    public String getCurrentWord() {
        StringBuilder sb = new StringBuilder();
        for (WordGameTile word : currentWordList) {
            sb.append(word.getWord());
        }
        return sb.toString().toLowerCase();
    }

    // flip current small board when clicked when it's phase two
    public void flipToNextPhase(int largePos, int smallPos) {
        Log.d(TAG, "This is a flip!");
        View rootView = mLargeWordGameTiles[largePos].getView();
        View phaseOne = rootView.findViewById(R.id.wordgame_small_1);
        View phaseTwo = rootView.findViewById(R.id.wordgame_small_2);
        WordGameFlipAnimation wordGameFlipAnimation = new WordGameFlipAnimation(phaseOne, phaseTwo);
        if (phaseOne.getVisibility() == View.GONE) {
            wordGameFlipAnimation.reverse();
        }
        rootView.startAnimation(wordGameFlipAnimation);
    }

    public void endCurrentGame() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Game ends!")
                .setMessage("Your score is: " + score)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getActivity(), WordGameMenuActivity.class);
                        getActivity().startActivity(intent);
                    }
                })
                .show();
    }
}
