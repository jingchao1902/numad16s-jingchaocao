package edu.neu.madcourse.jingchaocao.twoplayergame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.neu.madcourse.jingchaocao.R;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.CommunicationConstants;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.GcmNotification;

public class TwoPlayerLogActivity extends Activity {
    private static String TAG = TwoPlayerLogActivity.class.getSimpleName();
    private String boardInitializeInfo;
    TwoPlayerUser twoPlayerUser = new TwoPlayerUser();
    GoogleCloudMessaging gcm;
    Map<String, TwoPlayerUser> usersMap = new HashMap<>();
    private String wordString;
    private String wordListString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Firebase.setAndroidContext(this);
        final Firebase myFirebaseRef = new Firebase(CommunicationConstants.FIREBASE_URL);
        setContentView(R.layout.twoplayer_log_activity);
        ListView listView = (ListView) findViewById(R.id.twoplayer_log_wordlistview);
        //Button button = (Button) findViewById(R.id.clear_prefs);
        gcm = GoogleCloudMessaging.getInstance(this);

//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final SharedPreferences prefs = getGCMPreferences();
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.clear();
//                editor.apply();
//            }
//        });

        // Retrieve user list
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_singlechoice);

        // Need asynctask ?
        myFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userInfo : dataSnapshot.child("users").getChildren()) {
                    TwoPlayerUser twoPlayerUser = userInfo.getValue(TwoPlayerUser.class);
                    if (usersMap.get(twoPlayerUser.getName()) == null){
                        usersMap.put(twoPlayerUser.getName(), twoPlayerUser);
                        arrayAdapter.add(twoPlayerUser.getName());
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = arrayAdapter.getItem(position);
                TwoPlayerUser user = usersMap.get(name);
                sendGameRequest(CommunicationConstants.GAME_REQUEST_MSG, user.getRegId());
            }
        });

        if (getRegistrationId().isEmpty()) {
            // AlertDialog with EditText
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Type your name:");
            final EditText inputName = new EditText(this);
            builder.setView(inputName);
            builder.setPositiveButton(R.string.yes_label,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Editable name = inputName.getText();
                            twoPlayerUser.setName(name.toString());
                            registerInBackground();
                        }
                    });
            builder.setNegativeButton(R.string.no_label,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    });
            builder.show();
        }
    }

    public String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("registration_id", "");
    }

    public String getRegistrationName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("registration_name", "");
    }

    public SharedPreferences getGCMPreferences() {
        return this.getSharedPreferences(TwoPlayerLogActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    public void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    twoPlayerUser.setRegId(gcm.register(CommunicationConstants.GCM_SENDER_ID));
                    // implementation to store and keep track of registered devices here
                    msg = "Device registered, registration ID=" + twoPlayerUser.getRegId();
                    sendRegistrationInfoToBackend();
                    storeRegistrationInfo();
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }

    public void sendRegistrationInfoToBackend() {
        final Firebase myFirebaseRef = new Firebase(CommunicationConstants.FIREBASE_URL);
        Firebase ref = myFirebaseRef.child("users").child(twoPlayerUser.getName());
        twoPlayerUser.setHighestScore("");
        ref.setValue(twoPlayerUser);
    }

    public void storeRegistrationInfo() {
        final SharedPreferences prefs = getGCMPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("registration_id", twoPlayerUser.getRegId());
        editor.putString("registration_name", twoPlayerUser.getName());
        editor.apply();
    }

    public void sendGameRequest(final String message, String id) {
        final String reg_device = id;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Notification");
                msgParams.put("data.titleText", "Notification Title");
                msgParams.put("data.contentText", message);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                msgParams.put("data.senderID", getRegistrationId());
                msgParams.put("data.senderName", getRegistrationName());
                msgParams.put("data.gameboard", boardInitializeInfo);
                Log.d(TAG, "Sending content: " + message);
                Log.d(TAG, "Sender ID: " + getRegistrationId());
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        getApplicationContext());
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
