package edu.neu.madcourse.jingchaocao.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.content.Intent;
import android.widget.Button;

import edu.neu.madcourse.jingchaocao.R;
import edu.neu.madcourse.jingchaocao.communication.CommunicationActivity;
import edu.neu.madcourse.jingchaocao.dictionary.DictionaryActivity;
import edu.neu.madcourse.jingchaocao.memowords.MemoWordsMainActivity;
import edu.neu.madcourse.jingchaocao.memowords.MemoWordsTestActivity;
import edu.neu.madcourse.jingchaocao.twoplayergame.TwoPlayerGameActivity;
import edu.neu.madcourse.jingchaocao.twoplayergame.TwoPlayerMenuActivity;
import edu.neu.madcourse.jingchaocao.utt.TicTacActivity;
import edu.neu.madcourse.jingchaocao.wordgame.WordGameMenuActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


        // about button
        Button about_button = (Button) findViewById(R.id.about_button);
        about_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AboutButtonActivity.class);
                startActivity(intent);
            }
        });

        // generate error button
        Button error_button = (Button) findViewById(R.id.error_button);
        error_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                startActivity(intent);
            }
        });

        // Quit button
        Button quit_button = (Button) findViewById(R.id.quit_button);
        quit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });

        // Tic Tac Toe button
        Button tictactoe_button = (Button) findViewById(R.id.tictactoe_button);
        tictactoe_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TicTacActivity.class);
                startActivity(intent);
            }
        });

        // Dictionary button
        Button dictionary_button = (Button) findViewById(R.id.dictionary_button);
        dictionary_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, DictionaryActivity.class);
                startActivity(intent);
            }
        });

        // Word Game button
        Button wordgame_button = (Button) findViewById(R.id.wordgame_button);
        wordgame_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this,
                        WordGameMenuActivity.class);
                startActivity(intent);
            }
        });

        // Communication button
        Button communication_button = (Button) findViewById(R.id.communication_button);
        communication_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this,
                        CommunicationActivity.class);
                startActivity(intent);
            }
        });

        // Two player button
        Button two_player_button = (Button) findViewById(R.id.two_player_button);
        two_player_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this,
                        TwoPlayerMenuActivity.class);
                startActivity(intent);
            }
        });

        // Trickiest part button
        Button trikiest_part_button = (Button) findViewById(R.id.trikiest_part_button);
        trikiest_part_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        MemoWordsMainActivity.class);
                startActivity(intent);
            }
        });

        // Final project button
        Button final_project_button = (Button) findViewById(R.id.final_project_button);
        final_project_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Acknowledgements");
                alert.setMessage(R.string.final_project_acknow);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this,
                                MemoWordsMainActivity.class);
                        startActivity(intent);
                    }
                });
                alert.show();
            }
        });
    }
}
