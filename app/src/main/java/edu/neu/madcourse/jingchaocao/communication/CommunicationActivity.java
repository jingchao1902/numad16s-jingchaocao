package edu.neu.madcourse.jingchaocao.communication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.jingchaocao.R;

public class CommunicationActivity extends Activity{
    /* TwoPlayerConstants */
    private static final String GCM_SENDER_ID = "550530187683";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "GcmCommunication";
    private static final String REG_ID = "registration_id";
    private static final String APP_VERSION = "app_version";
    private static final String GCM_INTENT_FILTER = "gcm_intent_filter";
    private static final String APP_ACTIVE = "app_active";
    private static final String GCM_INTENT_DATA = "gcm_intent_data";
    private static final String OPPONENT_ID = "opponent_key";
    private static final String SELF_ID = "self_id";
    private static final String FULL_CONNECTION = "full_connection";
    private static final String NOTIFICATION_SET = "notification_set";
    private static final String FIREBASE_URL = "https://sizzling-inferno-1289.firebaseio.com/";
    private static final String SUCCESS_FLAG = "success_flag";

    /* Global variables */
    Context context;
    GoogleCloudMessaging gcm;
    String regId;
    BroadcastReceiver broadcastReceiver;
    User opponentUser;


    private TextView mTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.communication_activity);
        Button sendNameButton = (Button) findViewById(R.id.communication_send_name_button);
        Button sendMsgButton = (Button) findViewById(R.id.communication_send_message_button);
        Button acknowButton = (Button) findViewById(R.id.communication_acknow_button);
        Button backButton = (Button) findViewById(R.id.communication_back_button);
        //Button clearPrefsButton = (Button) findViewById(R.id.communication_clear_button);
        final EditText nameEditText = (EditText) findViewById(R.id.commnunication_enter_name_edit_text);
        final EditText msgEditText = (EditText) findViewById(R.id.commnunication_message_edit_text);
        mTextView = (TextView) findViewById(R.id.communication_text_view);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Toast.makeText(context,"Receiving message. Action is "+action,Toast.LENGTH_LONG).show();
                if (action.equals("com.google.android.c2dm.intent.RECEIVE")){
                    Log.d(TAG, "This is the broadcast receiver in the main activity!");
                    String intentData = intent.getStringExtra(GCM_INTENT_DATA);
                    Log.d(TAG, "Intent data from GCM: " + intent.getStringExtra(GCM_INTENT_DATA));
                    updateOpponentIDOnRegister(intent);
                    mTextView.setText("Get message from opponent: " + intent.getStringExtra(GCM_INTENT_DATA));
                }
            }
        };

        context = getApplicationContext();
        Firebase.setAndroidContext(context);


        /* Set click listeners */
//        clearPrefsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences prefs = getGcmPreferences(context);
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.clear();
//                editor.apply();
//            }
//        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        acknowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CommunicationActivity.this);
                builder.setMessage(R.string.communication_acknowledgements);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                builder.show();
            }
        });
        sendNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getGcmPreferences(context);
                boolean isConnected = prefs.getBoolean(FULL_CONNECTION, false);
                if (!isConnected){
                    String userId = nameEditText.getText().toString();
                    if (!userId.isEmpty()){
                        if (isInternetAvailable(context)){
                            // cannot use getText() in AsyncTask thread
                            final String name = nameEditText.getText().toString();
                            new AsyncTask<String,Integer, String>(){

                                @Override
                                protected String doInBackground(String... params) {
                                    User user = new User();
                                    user.setName(name);
                                    SharedPreferences prefs = getGcmPreferences(context);
                                    user.setRegId(prefs.getString(REG_ID, ""));
                                    sendCurrentUserToBackend(user);
                                    return SUCCESS_FLAG;
                                }
                                @Override
                                protected void onPostExecute(String result){
                                    super.onPostExecute(result);
                                    if(result.equals(SUCCESS_FLAG)){
                                        //checkWaitingListForUser();
                                        mTextView.setText("Name sent to server!");
                                    }else{
                                        mTextView.setText("Network is not available!");
                                    }
                                }
                            }.execute("");
                        }
                    }
                }
            }
        });

        sendMsgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetAvailable(context)){
                    String msgToSend = msgEditText.getText().toString();
                    if(!msgToSend.isEmpty()){
                        SharedPreferences prefs = getGcmPreferences(context);
                        String opponentID = prefs.getString(OPPONENT_ID, "");
                        if(opponentID.equals("")){
                            //mTextView.setText("Opponent ID not found!");
                            checkWaitingListForUser();
                        }else{
                            sendMessage(msgToSend, opponentID);
                            mTextView.setText("Message sent to opponent: " + opponentID);
                        }
                    }else{
                        mTextView.setText("Enter a non-empty string!");
                    }
                }else{
                    mTextView.setText("Internet is not available!");
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPlayServicesAvailable()){
            gcm = GoogleCloudMessaging.getInstance(context);
            regId = getRegistrationId(context);
            Log.d(TAG, "Registration ID found: " + regId);
            if (regId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.d(TAG, "Google Play Service is not available!");
        }

        // Register the broadcastReceiver,
        registerReceiver(broadcastReceiver, new IntentFilter("com.google.android.c2dm.intent.RECEIVE"));
        SharedPreferences prefs = getGcmPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(APP_ACTIVE, true);
        editor.apply();

        // what's this part for ?
        boolean isNotificationSet = prefs.getBoolean(NOTIFICATION_SET, false);
        Log.d(TAG, "Notification is set: " + isNotificationSet);
        if(isNotificationSet){
            Log.d(TAG, "After notification is send to Player one");
            Intent currentIntent = getIntent();
            Log.d(TAG, "currentIntent name: "+ currentIntent.getStringExtra(GCM_INTENT_DATA));
            //String bundleString = prefs.getString(NOTIFICATION_BUNDLE, "");
            updateOpponentIDOnRegister(currentIntent);
            editor.putBoolean(NOTIFICATION_SET, false);
            editor.apply();
        }




    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = getGcmPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(APP_ACTIVE, false);
        editor.apply();
        unregisterReceiver(broadcastReceiver);
    }


    public void checkWaitingListForUser(){
        if(isInternetAvailable(context)){
            new AsyncTask<String,Integer, User>(){
                boolean isAvailable = false;
                @Override
                protected User doInBackground(String... params) {
                    User user = new User();
                    if (getCurrentUserListFromBackend() != null){
                        user = getCurrentUserListFromBackend();
                        Log.d(TAG, "Current user in backend: " + user.getName());
                        isAvailable = true;
                    } else {
                        Log.d(TAG, "Current user list is empty!");
                    }
                    return user;
                }

                @Override
                protected void onPostExecute(User result){
                    super.onPostExecute(result);
                    if(isAvailable){
                        // Should put OPPONENT_GCM_ID here ?
                        String opponentID = result.getRegId();
                        if(!opponentID.equals(regId)){
                            Log.d(TAG, "Waiting user exists");
                            storeOtherRegistrationId(context, opponentID);
                            mTextView.setText("Successfully connected with opponent!");
                            //acceptRequestForCom(opponentID);
                        }else{
                            mTextView.setText("Waiting for someone...");
                        }
                    } else {
                        Log.d(TAG, "Waiting user not found");
                    }
                }
            }.execute("");
        }else{
            mTextView.setText("No network available!");
        }
    }

    // What's this for ?
    private void storeOtherRegistrationId(Context context, String id){
        final SharedPreferences prefs = getGcmPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Log.d(TAG, "Opponent Registration " + id);
        editor.putString(OPPONENT_ID, id);
        editor.apply();
    }

    public void acceptRequestForCom(String msg){
        if(isInternetAvailable(context)){
            new AsyncTask<String,Integer, String>(){

                private String param = null;

                @Override
                protected String doInBackground(String... params) {
                    param = params[0];
                    // clear currentUser
                    final Firebase myFirebaseRef = new Firebase(FIREBASE_URL);
                    myFirebaseRef.child("currentUser").removeValue();
                    return SUCCESS_FLAG;
                }

                @Override
                protected void onPostExecute(String result){
                    super.onPostExecute(result);
                    if(result.equals(SUCCESS_FLAG)){
                        mTextView.setText("Successfully connected with opponent");
                        SharedPreferences prefs = getGcmPreferences(context);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean(FULL_CONNECTION, true);
                        editor.apply();
                        Log.d(TAG, "Param content when accept request: " + param);
                        // What is this param ?
                        sendMessage(param, OPPONENT_ID);
                    }else{
                        mTextView.setText("Something went haywire!!!");
                    }
                }

            }.execute(msg);
        }else{
            mTextView.setText("No network available!");
        }
    }

    /**
     *
     * @param message The text content to send
     * @param id The receiver's gcm id
     */
    public void sendMessage(final String message, String id) {
        final String reg_device = id;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.gcm_intent_data", message);
                msgParams.put("data.self_id", getRegistrationId(context));
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        CommunicationActivity.this);
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(CommunicationActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    /**
     * To indicate the opponent is connected successfully, which means
     * this player has received messages from the opponent.
     * @param intent Intent from braodcastReceiver
     */
    private void updateOpponentIDOnRegister(Intent intent) {
        if(!intent.getStringExtra(SELF_ID).isEmpty()){
            SharedPreferences prefs = getGcmPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(OPPONENT_ID, intent.getStringExtra(SELF_ID));
            editor.putBoolean(FULL_CONNECTION, true);
            editor.apply();
            mTextView.setText("Opponent connected succesfully!");
        } else {
            Log.d(TAG, "Opponent ID not found!");
        }
    }


    public void sendCurrentUserToBackend(User userInfo){
        final Firebase myFirebaseRef = new Firebase(FIREBASE_URL);
        Firebase ref = myFirebaseRef.child("currentUser");
        ref.setValue(userInfo);
    }

    public User getCurrentUserListFromBackend(){
            final Firebase myFirebaseRef = new Firebase(FIREBASE_URL);
        myFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot userInfo : dataSnapshot.child("currentUser").getChildren()) {
//                    TwoPlayerUser user = userInfo.getValue(TwoPlayerUser.class);
//                    usersMap.put(user.getRegId(), user);
//                }
                opponentUser = dataSnapshot.child("currentUser").getValue(User.class);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                mTextView.setText("Current user not found!");
            }
        });
        return opponentUser;
    }
    /**
     *
     * @param context Application context
     * @return Registration ID in sharedPreferences
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.d(TAG, "Registration ID not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.d(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * Register the device for GCM, and store the ID.
     */
    private void registerInBackground() {
        if(isInternetAvailable(context)){
            Log.d(TAG, "hey inside registerInBackground");
            new AsyncTask<String,Integer,String>() {
                @Override
                protected String doInBackground(String... params) {
                    String msg = "";
                    try {
                        if (gcm == null) {
                            gcm = GoogleCloudMessaging.getInstance(context);
                        }
                        regId = gcm.register(GCM_SENDER_ID);
                        msg = "Device registered for the first time!";
                        storeRegistrationId(context, regId);
                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                    }
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                    mTextView.append(msg + "\n");
                }

            }.execute(null, null, null);
        }else{
            mTextView.setText("Network is not available!");
        }
    }


    /**
     * Store the registration ID into sharedPreferences
     * @param context Application context
     * @param id Registration ID from GCM when registered for the first time
     */
    private void storeRegistrationId(Context context, String id) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.d(TAG, "Saving registration ID on app version " + appVersion + "...");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(REG_ID, id);
        editor.putInt(APP_VERSION, appVersion);
        editor.apply();
    }

    /**
     *
     * @param context Application context
     * @return The sharedPreferences stored as the name of this class
     */
    private SharedPreferences getGcmPreferences(Context context) {
        return getSharedPreferences(CommunicationActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }


    /**
     *
     * @param context Application context
     * @return App version
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    /**
     *
     * @param context Application context
     * @return Availability of Internet
     */
    private static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return info != null;
    }

    /**
     *
     * @return Availability of play service
     */
    private boolean isPlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.d(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
