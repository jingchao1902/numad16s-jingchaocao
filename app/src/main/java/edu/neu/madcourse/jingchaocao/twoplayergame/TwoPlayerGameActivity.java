package edu.neu.madcourse.jingchaocao.twoplayergame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import edu.neu.madcourse.jingchaocao.R;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.CommunicationConstants;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.GcmNotification;

public class TwoPlayerGameActivity extends Activity{
    private TwoPlayerGameFragment mTwoPlayerGameFragment;
    private MediaPlayer mMediaPlayer;
    private boolean isActive = true;
    public static final String PREF_RESTORE = "pref_restore";
    private static String TAG = TwoPlayerGameActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twoplayer_play_activity);
        mTwoPlayerGameFragment = (TwoPlayerGameFragment) getFragmentManager()
                .findFragmentById(R.id.twoplayer_fragment_play_board);
        ImageView soundImgView = (ImageView) findViewById(R.id.twoplayer_sound_image);
        Log.d(TAG, mTwoPlayerGameFragment.toString());
        Log.d(TAG, soundImgView.toString());
        soundImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMediaPlayer.stop();
            }
        });
        ImageView pauseImgView = (ImageView) findViewById(R.id.twoplayer_pause_image);
        pauseImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isActive) {
                    onPause();
                } else {
                    onResume();
                }
            }
        });

        // For communication
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (!extras.isEmpty()){
            String currentPlayer = extras.getString("currentPlayerName");
            if (currentPlayer != null){
                if (currentPlayer.equals(getRegistrationName())){
                    mTwoPlayerGameFragment.currentWordView.setText("It's your turn!");
                } else if (currentPlayer.equals(getOpponentName())){
                    mTwoPlayerGameFragment.currentWordView.setText("It's " + getOpponentName() + "'s turn!");
                    //mTwoPlayerGameFragment.unEnableEntireBoard();
                }
            }
            // Generate the array for himself
//            String boardInfo = extras.getString("boardInfo");
//            if (boardInfo != null){
//                Log.d(TAG, "Board info is: " + boardInfo);
//                Bundle bundle = new Bundle();
//                bundle.putString("nineWords", boardInfo);
//                mTwoPlayerGameFragment.setArguments(bundle);
//                //mTwoPlayerGameFragment.setWordsToFill(boardInfo.split("#"));
//                for (int i = 0; i < 9; i++){
//                    Log.d(TAG, mTwoPlayerGameFragment.ninewordsToFill[i]);
//                }
//            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // uncopyrighted music from DesiredMusic on soundcloud.com
        mMediaPlayer = MediaPlayer.create(TwoPlayerGameActivity.this, R.raw.background_audio_game);
        mMediaPlayer.setVolume(0.5f, 0.5f);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }
    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        //mTwoPlayerGameFragment.countDownTimer.cancel();
        String gameData = mTwoPlayerGameFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE, gameData)
                .commit();
        Log.d("UT3", "state = " + gameData);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void submitWord(){
        mTwoPlayerGameFragment.submitWord();
    }

    public String getRegistrationName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("registration_name", "");
    }

    public String getOpponentName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("opponent_name", "");
    }

    public SharedPreferences getGCMPreferences() {
        return this.getSharedPreferences(TwoPlayerLogActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }
}
