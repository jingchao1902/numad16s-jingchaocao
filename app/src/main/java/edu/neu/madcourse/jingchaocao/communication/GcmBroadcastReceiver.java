package edu.neu.madcourse.jingchaocao.communication;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.GcmIntentService;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	private static final String TAG = "GcmCommunication";
	@Override
	public void onReceive(Context context, Intent intent) {
		ComponentName comp = new ComponentName(context.getPackageName(),
				GcmIntentService.class.getName());

		Log.d(TAG, "Inside GcmBroadcastReceiver of " + context.getPackageName());


		startWakefulService(context, (intent.setComponent(comp)));
		setResultCode(Activity.RESULT_OK);
	}
}
