package edu.neu.madcourse.jingchaocao.twoplayergame;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.jingchaocao.R;


public class TwoPlayerControlFragment extends Fragment {
    private TwoPlayerGameFragment mTwoPlayerGameFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.twoplayer_play_control_fragment, container, false);
        View confirm = rootView.findViewById(R.id.twoplayer_button_confirm);
        View main = rootView.findViewById(R.id.twoplayer_button_return);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
                mTwoPlayerGameFragment = (TwoPlayerGameFragment) getActivity().getFragmentManager()
                        .findFragmentById(R.id.twoplayer_fragment_play_board);
                mTwoPlayerGameFragment.countDownTimer.cancel();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((TwoPlayerGameActivity) getActivity()).submitWord();
            }
        });
        return rootView;
    }
}