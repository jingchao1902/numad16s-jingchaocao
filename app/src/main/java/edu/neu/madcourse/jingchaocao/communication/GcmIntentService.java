package edu.neu.madcourse.jingchaocao.communication;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import edu.neu.madcourse.jingchaocao.R;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.CommunicationConstants;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.GcmBroadcastReceiver;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    static final String TAG = "GcmCommunication";
    public static final String APP_ACTIVE = "isAppActive";
    public static final String NOTIFICATION_SET = "notificationSet";
    private static final String NOTIFICATION_BUNDLE = "notificationBundle";
    private static final String GCM_INTENT_FILTER = "gcm_intent_filter";
    private static final String GCM_INTENT_DATA = "gcm_intent_data";

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        Log.d("Receive message: " + String.valueOf(extras.size()), extras.toString());
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            Log.d(TAG, "Inside extras!");
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(),"");
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString(),"");
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                String value = extras.toString();
                SharedPreferences prefs = getGCMPreferences();
                boolean isAppActive = prefs.getBoolean(APP_ACTIVE, false);
                Log.d(TAG, "App Active status" + isAppActive);
                if(isAppActive){
                    sendBroadcastToCommunicationActivity(value);
                }else{
                    sendNotification("Received new message from Opponent... ",value);
                }
            }
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void sendBroadcastToCommunicationActivity(String value){
        Intent intent = new Intent();
        intent.setAction(GCM_INTENT_FILTER);
        intent.putExtra(GCM_INTENT_DATA, value);
        this.sendBroadcast(intent);
    }

    private void sendNotification(String msg,String value) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent comMainActivityIntent = new Intent(this,CommunicationActivity.class);
        comMainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        comMainActivityIntent.putExtra(GCM_INTENT_DATA, value);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,comMainActivityIntent, PendingIntent.FLAG_CANCEL_CURRENT);


        SharedPreferences prefs = getGCMPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(NOTIFICATION_SET, true);
        editor.putString(NOTIFICATION_BUNDLE, value);
        editor.apply();

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("GCM Notification")
                        .setSmallIcon(R.drawable.ic_stat_cloud)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setAutoCancel(true)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences(CommunicationActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
}