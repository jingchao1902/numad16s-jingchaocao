package edu.neu.madcourse.jingchaocao.twoplayergame;

public class TwoPlayerUser {
    private String regId;
    private String name;
    private String highestScore;

    public TwoPlayerUser() {
    }

    public TwoPlayerUser(String name) {
        this.name = name;
    }

    public void setHighestScore(String highestScore){
        this.highestScore = highestScore;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHighestScore(){
        return highestScore;
    }
    public String getRegId() {
        return regId;
    }

    public String getName() {
        return name;
    }
}
