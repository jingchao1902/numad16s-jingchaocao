package edu.neu.madcourse.jingchaocao.twoplayergame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import edu.neu.madcourse.jingchaocao.R;
import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.CommunicationConstants;

public class TwoPlayerMenuActivity extends Activity{
    private AlertDialog acknowDialog;
    private AlertDialog helpDialog;
    private String wordString;
    private String wordListString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twoplayer_menu_activity);

        // new game button
        Button new_button = (Button) findViewById(R.id.twoplayer_new_button);
        new_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                if (cm.getActiveNetworkInfo() != null) {
                    Intent intent = new Intent(TwoPlayerMenuActivity.this, TwoPlayerLogActivity.class);
                    startActivity(intent);
                } else {
                    new AlertDialog.Builder(TwoPlayerMenuActivity.this)
                            .setTitle("Error!")
                            .setMessage("Internet is not available")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
            }
        });

        // help button
        final Button help_button = (Button) findViewById(R.id.twoplayer_help_button);
        help_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TwoPlayerMenuActivity.this);
                builder.setMessage(R.string.wordgame_help_message);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                helpDialog = builder.show();
            }
        });

        // acknow button
        Button acknow_button = (Button) findViewById(R.id.twoplayer_about_button);
        acknow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TwoPlayerMenuActivity.this);
                builder.setMessage(R.string.twoplayer_acknowledgements);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                acknowDialog = builder.show();
            }
        });

        Button quit_button = (Button) findViewById(R.id.twoplayer_quit_button);
        quit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


}
