package edu.neu.madcourse.jingchaocao.twoplayergame;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.neu.madcourse.jingchaocao.twoplayergame.gcm.*;

import edu.neu.madcourse.jingchaocao.R;

public class TwoPlayerGameFragment extends Fragment {
    // Sensor
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    // Communication
    GoogleCloudMessaging gcm;
    String regid;
    BroadcastReceiver broadcastReceiver;
    private String connection;

    private static final String TAG = "WordGameFragment";
    // dictionary part
    private String wordString;
    private String wordListString;
    SharedPreferences msharedPreferences;
    SharedPreferences msharedPreferencesForFillingWords;
    public int[] ninewordsToFill = {12830,400000,120000,231230,441231,300000,100000,600000,310000};

    //store current word
    private List<TwoPlayerTile> currentWordList = new ArrayList<>();

    private TwoPlayerTile mEntireBoard = new TwoPlayerTile(this);
    private TwoPlayerTile mLargeTwoPlayerTiles[] = new TwoPlayerTile[9];
    private TwoPlayerTile mSmallTwoPlayerTiles[][] = new TwoPlayerTile[9][9];
    private ListView wordListView;
    TextView currentWordView;
    private List<List<TwoPlayerTile>> foundWordList = new ArrayList<>();
    private List<String> foundWordStrList = new ArrayList<>();
    ArrayAdapter mAdapter;
    CountDownTimer countDownTimer;
    private long leftTime;

    // store IDs of small and large tiles
    static private int mLargeIds[] = {R.id.twoplayer_large1, R.id.twoplayer_large2, R.id.twoplayer_large3,
            R.id.twoplayer_large4, R.id.twoplayer_large5, R.id.twoplayer_large6, R.id.twoplayer_large7, R.id.twoplayer_large8,
            R.id.twoplayer_large9,};
    static private int mSmallIds[] = {R.id.twoplayer_small1, R.id.twoplayer_small2, R.id.twoplayer_small3,
            R.id.twoplayer_small4, R.id.twoplayer_small5, R.id.twoplayer_small6, R.id.twoplayer_small7, R.id.twoplayer_small8,
            R.id.twoplayer_small9,};

    // pattern for filling longest word_unselected
    private int[] longestWordIndicesArr = {0, 7, 8, 1, 6, 5, 2, 3, 4};

    // scoring part
    private TextView scoreTextView;
    private int score = 0;
    private List<Character> pointOne = new ArrayList<>(Arrays.asList('e', 'a', 'i', 'o', 'n', 'r', 't', 'l', 's'));
    private List<Character> pointTwo = new ArrayList<>(Arrays.asList('d', 'g'));
    private List<Character> pointThree = new ArrayList<>(Arrays.asList('b', 'c', 'm'));
    private List<Character> pointFour = new ArrayList<>(Arrays.asList('f', 'h', 'v', 'w', 'y'));
    private List<Character> pointFive = new ArrayList<>(Arrays.asList('k'));
    private List<Character> pointEight = new ArrayList<>(Arrays.asList('j', 'x'));
    private List<Character> pointTen = new ArrayList<>(Arrays.asList('q', 'z'));

    // available adjacent words
    private int[][] adjacentWordArr = {{1, 3, 4},
            {0, 2, 3, 4, 5},
            {1, 4, 5},
            {0, 1, 4, 6, 7},
            {0, 1, 2, 3, 5, 6, 7, 8},
            {1, 2, 4, 7, 8},
            {3, 4, 7},
            {3, 4, 5, 6, 8},
            {4, 5, 7}};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGame();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Bundle extras = intent.getExtras();
                if (action.equals("com.google.android.c2dm.intent.RECEIVE")){
                    if (!extras.isEmpty()){
                        String content = extras.getString("contentText");
                        String move = extras.getString("moveText");
                        String foundWord = extras.getString("foundWord");
                        String currentPlayer = extras.getString("currentPlayerName");
                        String boardInfo = extras.getString("boardInfo");
                        if (currentPlayer != null){
                            if (currentPlayer.equals(getRegistrationName())){
                                currentWordView.setText("It's your turn!");
                            }
                        }
                        if (move != null){
                            if (move.equals("shake")){
                                currentWordView.setText(getOpponentName() + " needs your help!");
                            } else if (move.equals("end")){
                                endCurrentGame();
                            } else {
                                Log.d(TAG, move);
                                int largePos = Character.getNumericValue(move.charAt(0));
                                for (int i = 1; i < move.length(); i++){
                                    int smallPos = Character.getNumericValue(move.charAt(i));
                                    View view = mSmallTwoPlayerTiles[largePos][smallPos].getView();
                                    view.setBackgroundResource(R.drawable.word_selected);
                                    mSmallTwoPlayerTiles[largePos][smallPos].setClickedState();
                                    view.setOnClickListener(null);
                                }
                                for (int i = 0; i < 9; i++){
                                    if (!mSmallTwoPlayerTiles[largePos][i].getClickedState()){
                                        mSmallTwoPlayerTiles[largePos][i].setClickedState();
                                        TextView view = (TextView) mSmallTwoPlayerTiles[largePos][i].getView();
                                        view.setOnClickListener(null);
                                        view.setText("");
                                    }
                                }
                                Log.d(TAG, "This is a move message: " + move);
                            }
                        }
                        if (foundWord != null){
                            foundWordStrList.add(foundWord);
                            calculateScore(foundWord);
                            Log.d(TAG, "This is a found word: " + foundWord);
                        }

                    }
                    Log.d(TAG, "This is the broadcast receiver in the game fragment!");
                }
            }
        };
        Log.d(TAG, "This is the onCreate of fragment");
        Firebase.setAndroidContext(getActivity());
        mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setListener(new ShakeDetector.ShakeListener() {

            @Override
            public void onShake() {
                sendMoveToOpponent("shake", "", getOpponentId());
                currentWordView.setText("Sent help to " + getOpponentName());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.twoplayer_play_board_fragment, container, false);
        mAdapter = new ArrayAdapter<>(getActivity(), R.layout.wordgame_found_word_list, foundWordStrList);
        wordListView = (ListView) rootView.findViewById(R.id.twoplayer_wordlistview);
        wordListView.setAdapter(mAdapter);
        wordListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                wordListView.setSelection(mAdapter.getCount() - 1);
            }
        });
        currentWordView = (TextView) rootView.findViewById(R.id.twoplayer_current_word);
        //currentWordView.setText("Game begins!");
        scoreTextView = (TextView) rootView.findViewById(R.id.twoplayer_score_text);
        scoreTextView.setText("Score: " + score);
        getDictionary();
        timerStart(rootView);
        initViews(rootView);
        return rootView;
    }
    private void initViews(final View rootView) {
        mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            View outer = rootView.findViewById(mLargeIds[large]);
            mLargeTwoPlayerTiles[large].setView(outer);
            mLargeTwoPlayerTiles[large].setLargePos(large);
            Log.d(TAG, "List String length: " + wordListString.length());
            //int randomIndex = new Random().nextInt(wordListString.length() / 10);
            int randomIndex = ninewordsToFill[large] / 10;
            int start = randomIndex * 10;
            StringBuilder sb = new StringBuilder();
            for (int i = start; i < start + 10; i++) {
                sb.append(wordListString.charAt(i));
            }
            String longestWord = sb.toString();
            //Log.d(TAG, longestWord);
            for (int small = 0; small < 9; small++) {
                TextView inner = (TextView) outer.findViewById
                        (mSmallIds[small]);
                inner.setText(Character.toString(longestWord.charAt(longestWordIndicesArr[small])).
                        toUpperCase());
                final TwoPlayerTile smallTwoPlayerTile = mSmallTwoPlayerTiles[large][small];
                final int fLarge = large;
                final int fSmall = small;
                smallTwoPlayerTile.setLargePos(large);
                smallTwoPlayerTile.setSmallPos(small);
                smallTwoPlayerTile.setWord(Character.toString(longestWord.charAt(longestWordIndicesArr[small])).
                        toUpperCase());
                smallTwoPlayerTile.setView(inner);

                // Communication part
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!smallTwoPlayerTile.getClickedState()) {
                            if (currentWordList.size() == 0) {
                                currentWordList.add(smallTwoPlayerTile);
                                displayCurrentWord();
                                view.setBackgroundResource(R.drawable.word_selected);
                                smallTwoPlayerTile.setClickedState();
                                return;
                            } else if (fLarge != currentWordList.get(0).getLargePos()) {
                                currentWordView.setText("Tap on one board!");
                                return;
                            } else if (fLarge == currentWordList.get(0).getLargePos() && !isLetterAjacent(fSmall)) {
                                currentWordView.setText("Tap on adjacent word!");
                                return;
                            }
                            currentWordList.add(smallTwoPlayerTile);
                            displayCurrentWord();
                            view.setBackgroundResource(R.drawable.word_selected);
                            smallTwoPlayerTile.setClickedState();
                        } else {
                            int index = currentWordList.indexOf(mSmallTwoPlayerTiles[fLarge][fSmall]);
                            Log.d(TAG, "index is: " + index);
                            Log.d(TAG, "list size is " + currentWordList.size());
                            List<TwoPlayerTile> listAfterCurrent = new ArrayList<>();
                            for (int i = index; i < currentWordList.size(); i++) {
                                currentWordList.get(i).setClickedState();
                                int largePos = currentWordList.get(i).getLargePos();
                                int smallPos = currentWordList.get(i).getSmallPos();
                                TextView txtView = (TextView) mLargeTwoPlayerTiles[largePos].getView().findViewById(mSmallIds[smallPos]);
                                txtView.setBackgroundResource(R.drawable.word_unselected);
                                listAfterCurrent.add(currentWordList.get(i));
                            }
                            currentWordList.removeAll(listAfterCurrent);
                            Log.d(TAG, "list size is " + currentWordList.size());
                            displayCurrentWord();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mShakeDetector);
        getActivity().unregisterReceiver(broadcastReceiver);
        SharedPreferences msharedPreferences;
        msharedPreferences = getActivity().getSharedPreferences("wordgame_sharedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = msharedPreferences.edit();
        editor.putString("wordString", wordString);
        editor.apply();

        SharedPreferences msharedPreferencesForFillingWords;
        msharedPreferencesForFillingWords = getActivity().getSharedPreferences("wordgame_sharedPreferences_fill", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorForFillingWords = msharedPreferencesForFillingWords.edit();
        editorForFillingWords.putString("wordListString", wordListString);
        editorForFillingWords.apply();

        //sendMoveToOpponent("disconnected", "", getOpponentId());
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    // initialize mEntireBoard, mLargeTwoPlayerTiles, mSmallTwoPlayerTiles
    // bound this game fragment with each tile in this game
    public void initGame() {
        Log.d("UT3", "init game");
        mEntireBoard = new TwoPlayerTile(this);
        // Create all the tiles
        for (int large = 0; large < 9; large++) {
            mLargeTwoPlayerTiles[large] = new TwoPlayerTile(this);
            for (int small = 0; small < 9; small++) {
                mSmallTwoPlayerTiles[large][small] = new TwoPlayerTile(this);
            }
            mLargeTwoPlayerTiles[large].setSubTiles(mSmallTwoPlayerTiles[large]);
        }
        mEntireBoard.setSubTiles(mLargeTwoPlayerTiles);
    }

    public String getState() {
        return "";
    }

    public void submitWord() {
        if (currentWordList.size() == 0) {
            currentWordView.setText("Please select a word!");
            return;
        }
        if (currentWordList.size() < 3) {
            currentWordView.setText("Words' length smaller than 3");
            clearSelectedState();
            currentWordList.clear();
            return;
        }
        String word = getCurrentWord();
        String newLine = "\n";
        String wordToCheck = newLine.concat(word.concat("\n"));
        Log.d(TAG, word);
        Log.d(TAG, wordToCheck);
        if (wordString.contains(wordToCheck)) {
            // add the word into the listview
            StringBuilder sb = new StringBuilder();
            for (TwoPlayerTile twoPlayerTile : currentWordList) {
                sb.append(twoPlayerTile.getWord().toLowerCase());
            }
            if (!foundWordStrList.contains(sb.toString())) {
                // Need to copy the arraylist
                foundWordList.add(new ArrayList<>(currentWordList));
                foundWordStrList.add(sb.toString());
                //mAdapter = new ArrayAdapter<>(getActivity(), R.layout.wordgame_found_word_list, foundWordStrList);
                //wordListView.setAdapter(mAdapter);
                calculateScore(word);
                unEnableSubmittedBoard();
                currentWordView.setText("It's " + getOpponentName() + "'s turn!");

                // For communication
                StringBuilder moveSb = new StringBuilder();
                moveSb.append(currentWordList.get(0).getLargePos());
                for (TwoPlayerTile twoPlayerTile : currentWordList) {
                    moveSb.append(twoPlayerTile.getSmallPos());
                }
                sendMoveToOpponent(moveSb.toString(), sb.toString(), getOpponentId());
//                if (connection.equals("disconnected")){
//                    sendReconnectRequest(moveSb.toString(), sb.toString(), getOpponentId(), "reconnect");
//                } else {
//                    sendMoveToOpponent(moveSb.toString(), sb.toString(), getOpponentId());
//                }
            } else {
                currentWordView.setText("Word already found!");
                clearSelectedState();
                currentWordList.clear();
            }
        } else {
            clearSelectedState();
            currentWordView.setText(R.string.wordgame_word_notfound_text);
            scoreTextView.setText("Score: " + Integer.toString(score -= 1));// penalty for invalid word submission
        }
        // which list ?
        if (foundWordStrList.size() == 9) {
            sendMoveToOpponent("end", "", getOpponentId());
            endCurrentGame();
        }
        currentWordList.clear();
    }

    public void calculateScore(String word) {
        // if we find a word of length 9 in phase two, then we add 15 points to score.

        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);
            if (pointOne.contains(letter)) {
                score += 1;
            } else if (pointTwo.contains(letter)) {
                score += 2;
            } else if (pointThree.contains(letter)) {
                score += 3;
            } else if (pointFour.contains(letter)) {
                score += 4;
            } else if (pointFive.contains(letter)) {
                score += 5;
            } else if (pointEight.contains(letter)) {
                score += 8;
            } else if (pointTen.contains(letter)) {
                score += 10;
            }
        }
        scoreTextView.setText("Score: " + score);
    }

    public void clearSelectedState() {
        for (TwoPlayerTile word : currentWordList) {
            int largePos = word.getLargePos();
            int smallPos = word.getSmallPos();
            mSmallTwoPlayerTiles[largePos][smallPos].getView().setBackgroundResource(R.drawable.word_unselected);
            mSmallTwoPlayerTiles[largePos][smallPos].setClickedState();
        }
    }

    public void timerStart(final View rootView) {
        countDownTimer = new CountDownTimer(120000, 1000) {
            TextView view = (TextView) rootView.findViewById(R.id.twoplayer_timer_text);

            public void onTick(long millisUntilFinished) {
                leftTime = millisUntilFinished;
                Log.d(TAG, "Time left: " + millisUntilFinished);
                if (millisUntilFinished <= 11000 && millisUntilFinished >= 10000) {
                    currentWordView.setText("Only ten seconds left!");
                }
                view.setText("Time: " + formatMilliSecondsToTime(millisUntilFinished));
            }

            public void onFinish() {
                // why can't use getActivity() here ?
                new AlertDialog.Builder(getActivity())
                        .setTitle("Time is out")
                        .setMessage("Enter a new game!")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), TwoPlayerMenuActivity.class);
                                getActivity().startActivity(intent);
                            }
                        })
                        .show();
            }
        }.start();
    }

    private String formatMilliSecondsToTime(long milliseconds) {

        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        return twoDigitString(minutes) + " : " + twoDigitString(seconds);
    }

    private String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    public boolean isLetterAjacent(int small) {
        int lastPos = currentWordList.get(currentWordList.size() - 1).getSmallPos();
        int[] currentAvailablePos = adjacentWordArr[lastPos];
        List<Integer> currentAvailablePosList = new ArrayList<>();
        for (int pos : currentAvailablePos) {
            currentAvailablePosList.add(pos);
        }
        for (TwoPlayerTile word : currentWordList) {
            int smallPos = word.getSmallPos();
            Log.d(TAG, "Current small pos: " + small);
            currentAvailablePosList.remove(Integer.valueOf(smallPos));
        }
        return currentAvailablePosList.contains(small);
    }

    public void unEnableSubmittedBoard() {
        int largePos = currentWordList.get(0).getLargePos();
        for (TwoPlayerTile smallTwoPlayerTile : mSmallTwoPlayerTiles[largePos]) {
            if (!currentWordList.contains(smallTwoPlayerTile)) {
                int smallPos = smallTwoPlayerTile.getSmallPos();
                // must use findViewById method to get view type! otherwise cannot access the methods
                // inside this view
                TextView txtView = (TextView) mLargeTwoPlayerTiles[largePos].getView().findViewById(mSmallIds[smallPos]);
                txtView.setText("");
            }
        }
        // make the words unclickable
        for (int smallId : mSmallIds) {
            TextView txtView = (TextView) mLargeTwoPlayerTiles[largePos].getView().findViewById(smallId);
            txtView.setOnClickListener(null);
        }
    }

    public void displayCurrentWord() {
        currentWordView.setText(getCurrentWord());
    }

    public String getCurrentWord() {
        StringBuilder sb = new StringBuilder();
        for (TwoPlayerTile word : currentWordList) {
            sb.append(word.getWord());
        }
        return sb.toString().toLowerCase();
    }

    public void endCurrentGame() {
        final Firebase myFirebaseRef = new Firebase(CommunicationConstants.FIREBASE_URL);
        Firebase ref = myFirebaseRef.child("users").child(getRegistrationName());
        ref.child("highestScore").setValue(score);
        final StringBuilder sb = new StringBuilder();
        myFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userInfo : dataSnapshot.child("users").getChildren()) {
                    TwoPlayerUser twoPlayerUser = userInfo.getValue(TwoPlayerUser.class);
                    sb.append(twoPlayerUser.getName() + "   ").append(twoPlayerUser.getHighestScore())
                    .append('\n');
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        Log.d(TAG, "Score list: " + sb.toString());
        new AlertDialog.Builder(getActivity())
                .setTitle("Your score is: " + score)
                .setMessage(sb.toString())
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                })
                .show();
    }

    public void unEnableEntireBoard() {
        for (int largePos = 0; largePos < 9; largePos++){
            for (int smallId : mSmallIds) {
                TextView txtView = (TextView) mLargeTwoPlayerTiles[largePos].getView().findViewById(smallId);
                txtView.setOnClickListener(null);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("com.google.android.c2dm.intent.RECEIVE"));
        String moveMsg = getActivity().getIntent().getStringExtra(CommunicationConstants.GAME_MOVE_KEY);
        if (moveMsg != null) {
            for (int i = 1; i < moveMsg.length(); i++){
                //TextView txtView = (TextView) mLargeTwoPlayerTiles[moveMsg.charAt(0)].getView().findViewById();
            }
            Log.d("Received move: ", moveMsg);
        }

        // Send messages to allow asynchronous play

    }

    // For communication
    public void sendMoveToOpponent(final String move, final String foundWord, String id) {
        final String reg_device = id;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Notification");
                msgParams.put("data.titleText", "Notification Title");
                msgParams.put("data.moveText", move);
                msgParams.put("data.foundWord", foundWord);
                msgParams.put("data.currentPlayerName", getOpponentName());
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                //msgParams.put("data.currentPlayerName", getOpponentName());
                Log.d(TAG, "Sending move: " + move);
                Log.d(TAG, "Sending word: " + foundWord);
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        getActivity());
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }

    public String getOpponentId() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("opponent_id", "");
    }

    public String getOpponentName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("opponent_name", "");
    }

    public String getRegistrationName() {
        final SharedPreferences prefs = getGCMPreferences();
        return prefs.getString("registration_name", "");
    }

    public SharedPreferences getGCMPreferences() {
        return getActivity().getSharedPreferences(TwoPlayerLogActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    public void getDictionary() {
        SharedPreferences msharedPreferences;
        SharedPreferences msharedPreferencesForFillingWords;
        msharedPreferencesForFillingWords = getActivity().getSharedPreferences("wordgame_sharedPreferences_fill", Context.MODE_PRIVATE);
        msharedPreferences = getActivity().getSharedPreferences("wordgame_sharedPreferences", Context.MODE_PRIVATE);
        wordListString = msharedPreferencesForFillingWords.getString("wordListString", wordListString);
        wordString = msharedPreferences.getString("wordString", wordString);
        if (wordString == null) {
            long start = System.currentTimeMillis();
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.wordlist);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buff = new byte[3145728];
                // 3mb size -> 3145728 bytes (since there are 432334 words) We can estimate every word_unselected
                // has less than 10 length of characters
                for (int i; (i = inputStream.read(buff)) != -1; ) {
                    byteArrayOutputStream.write(buff, 0, i);
                }
                // Just store the wordlist in a long string, which can avoid using ArrayList
                wordString = byteArrayOutputStream.toString();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            long end = System.currentTimeMillis();
            double t = ((end - start) / 1000.0);
            System.out.println("read words list and take " + t + " seconds");
            String newLine = "\n";
            wordString = newLine.concat(wordString);
        }
        if (wordListString == null) {
            long start = System.currentTimeMillis();
            // how to transform the sharedPreferences string
            // use hashset ?
            System.out.println(wordString.length());
            try {
                BufferedReader bufReader = new BufferedReader(new StringReader(wordString));
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufReader.readLine()) != null) {
                    if (line.length() == 9) {
                        sb.append(line).append('\n');
                    }
                }
                wordListString = sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            long end = System.currentTimeMillis();
            double t = ((end - start) / 1000.0);
            System.out.println("read " + wordListString.length() + " words and take " + t + " seconds");
        }

    }


    public void sendReconnectRequest(final String move, final String foundWord, String id, final String connection) {
        final String reg_device = id;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Notification");
                msgParams.put("data.titleText", "Notification Title");
                msgParams.put("data.moveText", move);
                msgParams.put("data.foundWord", foundWord);
                msgParams.put("data.currentPlayerName", getOpponentName());
                msgParams.put("data.connection", connection);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                //msgParams.put("data.currentPlayerName", getOpponentName());
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        getActivity());
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }
}
