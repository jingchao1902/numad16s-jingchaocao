package edu.neu.madcourse.jingchaocao.twoplayergame.gcm;

public class CommunicationConstants
{
    public static String FIREBASE_URL = "https://sizzling-inferno-1289.firebaseio.com/";
    public static final String TAG = "GCM_Globals";
    public static final String GCM_SENDER_ID = "550530187683";
    public static final String BASE_URL = "https://android.googleapis.com/gcm/send";
    public static final String PREFS_NAME = "GCM_Communication";

    // replace it with your own gcm api key here
    public static final String GCM_API_KEY = "AIzaSyBMOAoBNEuFvTKKUIiTdEdTp6kx-hnOdLI";
    public static final int SIMPLE_NOTIFICATION = 22;
    public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks
    public static int mode = 0;

    public static String alertText = "";
    public static String titleText = "";
    public static String contentText = "";

    public static final String GAME_REQUEST_MSG = "game_request_msg";
    public static final String GAME_REQUEST_ACCEPT = "game_request_accept";
    public static final String ACCEPT_KEY = "accept_key";
    public static final String ACCEPT_SERVICE_MSG = "accept_service_msg";
    public static final String SENDER_ID_KEY = "sender_id_key";
    public static final String GAME_RECEIVED_SERVICE_MSG = "game_received_msg";
    public static final String GAME_RECEIVED_KEY = "game_received_key";
    public static final String GAME_MOVE_KEY = "game_move_key";
    public static final String TWO_PLAYER_MODE_FLAG = "two_player_mode_flag";
    public static final String TWO_PLAYER_MODE_KEY = "two_player_mode_key";
}